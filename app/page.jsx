import Hero from "@/main_apps/components/home/Hero";
import React, { Fragment } from "react";

export default function page() {
    return (
        <Fragment>
            <Hero />
        </Fragment>
    );
}
