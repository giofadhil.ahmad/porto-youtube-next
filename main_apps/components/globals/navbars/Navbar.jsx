"use client";

import Link from "next/link";
import React, {useState} from "react";
import Hamburger from "./icons/Hamburger";

export default function Navbar() {
    const [menu, setMenu] = useState(false);

    return (
        <div
            className={`
                bg-dark-blue
                text-white
                text-16
                md:text-18
                fixed
                top-0
                left-0
                w-full
                z-10
            `}
        >
            <nav className="max-w-1280 mx-auto px-24 py-24">
                <button
                    className="block md:hidden"
                    onClick={() => {
                        setMenu((prev) => {
                            return !prev;
                        });
                    }}
                >
                    <Hamburger />
                </button>
                {menu || window.innerWidth > 768 ? (
                    <div className="gap-24 flex flex-col md:flex-row mt-24 md:mt-0">
                        <Link href="/">Home</Link>
                        <Link href="/">Careers</Link>
                        <Link href="/">Blog</Link>
                        <Link href="/">About Us</Link>
                    </div>
                ) : null}
            </nav>
        </div>
    );
}
