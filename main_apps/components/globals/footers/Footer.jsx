import React from "react";
import Instagram from "./icons/instagram";
import LinkedIn from "./icons/LinkedIn";

export default function Footer() {
    return (
        <div className="bg-dark-blue text-white">
            <div className="max-w-1280 mx-auto px-24 py-48">
                <div className="flex flex-col md:flex-row gap-16 items-start md:items-center justify-between">
                    <div className="flex gap-8">
                        <Instagram />
                        <LinkedIn />
                    </div>
                    <div className="font-semibold tracking-2 uppercase">2023 DIRAIT - All Rights Reserved</div>
                </div>
            </div>
        </div>
    );
}
