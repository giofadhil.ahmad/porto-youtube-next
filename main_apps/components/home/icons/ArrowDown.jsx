import React from "react";

export default function ArrowDown() {
    return (
        <svg width="14" height="17" viewBox="0 0 14 17" fill="none" xmlns="http://www.w3.org/2000/svg">
            <path d="M7 0.5V16.5M7 16.5L13 10.5M7 16.5L1 10.5" stroke="#272D3D" strokeLinecap="round" strokeLinejoin="round" />
        </svg>
    );
}
