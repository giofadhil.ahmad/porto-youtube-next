import React from "react";
import ArrowDown from "./icons/ArrowDown";
import Image from "next/image";

export default function Hero() {
    return (
        <div
            className={`
              flex
              bg-black
              bg-opacity-75
              h-screen
              items-center
              justify-center
              relative
            `}
        >
            <Image fill src="/images/hero_background.jpg" alt="group of people" className="-z-10 object-cover" />
            <div className="mt-72 md:mt-75 text-center text-white px-20">
                <h6 className="lg:text-18 md:text-16 text-14 font-semibold">WHAT WE OFFER</h6>
                <h1 className="lg:text-64 md:text-48 text-32 font-bold mb-16">Our Agency, Is Your Online Solution</h1>
                <p className="lg:text-18 md:text-16 text-14 max-w-1100 mb-24">
                    We have expertise in creating online solution: in 2 years, we have maintain and develop more than 10 website project, we know all
                    the pitfalls very well. We will help you create a product for your budget
                </p>
                <div className="flex flex-col md:flex-row items-center justify-center gap-16">
                    <button
                        className={`
                          w-200
                          py-16
                          flex 
                          gap-8 
                          justify-center
                          bg-light-blue
                          transition-all
                          hover:bg-opacity-90
                        `}
                    >
                        Discuss my project
                    </button>
                    <button
                        className={`
                          w-200
                          py-16
                          bg-white
                          flex 
                          gap-8 
                          justify-center
                          text-dark-blue
                          transition-all
                          hover:bg-opacity-90
                        `}
                    >
                        Go Explore
                        <ArrowDown />
                    </button>
                </div>
            </div>
        </div>
    );
}
